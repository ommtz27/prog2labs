package labs.lab3.Abstraction;

import labs.lab3.Client;
import labs.lab3.Enums.rStatus;
import labs.lab3.Rental;
import labs.lab3.Reservation;

public class RentalL extends Lists<Rental>{
  @Override
  public int add(Rental rental) {
    for (Reservation reservation : arrayReservation) {
      if (rental.getReservationId() == reservation.getReservationId()) {
        for (Client client: arrayClient) {
          if (reservation.getClientId() == client.getClientId()) {
            client.getRentalRecord().add(rental);
            reservation.setReservationCompleted(true);
            reservation.setReservationStatus(rStatus.COMPLETED);
            arrayRental.add(rental);
            return 1;
          }
        }
      }
    }
    return 0;
  }

  @Override
  public int remove(int id) {
    return 0;
  }

  @Override
  public int edit(int id, Rental rental) {
    return 0;
  }
}
