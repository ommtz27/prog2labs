package labs.lab3.Abstraction;

import java.util.ArrayList;
import labs.lab3.Car;
import labs.lab3.Client;
import labs.lab3.Enums.Insurance;
import labs.lab3.Enums.rStatus;
import labs.lab3.Rental;
import labs.lab3.Reservation;

public abstract class Lists<T> {
  public ArrayList<Car> arrayCar;
  public ArrayList<Client> arrayClient;
  public ArrayList<Rental> arrayRental;
  public ArrayList<Reservation> arrayReservation;

  public Lists(){

    this.arrayClient = new ArrayList<>();
    this.arrayCar = new ArrayList<>();
    this.arrayRental = new ArrayList<>();
    this.arrayReservation = new ArrayList<>();

    var client1 = new Client(1, "John Doe", "555-1234", "123 Main St", 12345, "2020-01-01", 5);
    this.arrayClient.add(client1);
    var client2 = new Client(2, "Jane Smith", "555-5678", "456 Oak St", 67890, "2019-05-15", 3);
    this.arrayClient.add(client2);
    var client3 = new Client(3, "Bob Johnson", "555-9876", "789 Elm St", 54321, "2022-03-10", 2);
    this.arrayClient.add(client3);

    Car car1 = new Car(1, "Toyota", "Camry", "2022", "10,000 miles", true);
    this.arrayCar.add(car1);
    Car car2 = new Car(2, "Honda", "Civic", "2021", "15,000 miles", true);
    this.arrayCar.add(car2);
    Car car3 = new Car(3, "Ford", "Escape", "2020", "20,000 miles", false);
    this.arrayCar.add(car3);

    // Rellenar arrayReservation
    Reservation reservation1 = new Reservation(1, 1, "2022-01-15", "2022-02-01", 1, false, rStatus.WAITING);
    this.arrayReservation.add(reservation1);
    Reservation reservation2 = new Reservation(2, 2, "2022-02-20", "2022-03-05", 2, false, rStatus.WAITING);
    this.arrayReservation.add(reservation2);
    Reservation reservation3 = new Reservation(3, 3, "2022-03-05", "2022-04-01", 3, false, rStatus.WAITING);
    this.arrayReservation.add(reservation3);

    // Rellenar arrayRental
    Rental rental1 = new Rental(1, 1, "Standard Terms", "2022-02-01", "2022-02-10", 100.00, Insurance.LIABILITY_INSURANCE);
    this.arrayRental.add(rental1);
    Rental rental2 = new Rental(2, 2, "Extended Terms", "2022-03-05", "2022-03-15", 120.00, Insurance.COLLISION_DAMAGE_WAIVER);
    this.arrayRental.add(rental2);
    Rental rental3 = new Rental(3, 3, "Basic Terms", "2022-04-01", "2022-04-10", 90.00, Insurance.COMPREHENSIVE_INSURANCE);
    this.arrayRental.add(rental3);

  }


  public abstract int add(T t);
  public abstract int remove(int id);
  public abstract int edit(int id,T t);
}
