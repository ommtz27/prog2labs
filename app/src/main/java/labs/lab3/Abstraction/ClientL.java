package labs.lab3.Abstraction;

import labs.lab3.Client;

public class ClientL extends Lists<Client>{
  @Override
  public int add(Client client) {
    if (arrayClient.add(client)) {
      return 1;
    }
    return 0;
  }

  @Override
  public int remove(int id) {
    for (int i = 0; i < arrayClient.size(); i++) {
      if (arrayClient.get(i).getClientId() == id) {
        arrayClient.remove(i);
        return 1;
      }
    }
    return 0;
  }

  @Override
  public int edit(int id, Client client) {
    for (Client clients : arrayClient) {
      if (clients.getClientId() == id) {
        clients.setClientName(client.getClientName());
        clients.setClientPhone(client.getClientPhone());
        clients.setClientAddress(client.getClientAddress());
        clients.setLicenseId(client.getLicenseId());
        clients.setLicenseExpedition(client.getLicenseExpedition());
        clients.setLicenseYears(client.getLicenseYears());
        return 1;
      }
    }
    return 0;
  }
}
