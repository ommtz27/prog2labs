package labs.lab3.Abstraction;

import labs.lab3.Car;
import labs.lab3.Client;
import labs.lab3.Reservation;

public class ReservationL extends Lists<Reservation> {
  @SuppressWarnings("checkstyle:FinalParameters")
  @Override
  public int add(Reservation reservation) {
    for (Client client : arrayClient) {
      if (client.getClientId() == reservation.getClientId()) {
        arrayReservation.add(reservation);
        for (Car car : arrayCar) {
          if (car.getcarId() == reservation.getCarId()) {
            if (car.isAvailability()) {
              car.setAvailability(false);
              arrayReservation.add(reservation);
              return 1;
            }
          }
        }
      }
    }
    return 0;
  }

  @Override
  public int remove(int id) {
    for (int i = 0; i < arrayReservation.size(); i++) {
      if (arrayReservation.get(i).getReservationId() == id) {
        for (Car car : arrayCar) {
          if (car.getcarId() == arrayReservation.get(i).getCarId()) {
            car.setAvailability(true);
            arrayReservation.remove(i);
            return 1;
          }
        }
      }
    }
    return 0;
  }

  @Override
  public int edit(int id, Reservation reservation) {
    for (Reservation reservations : arrayReservation) {
      if (reservations.getReservationId() == id) {
        reservations.setClientId(reservation.getClientId());
        reservations.setReservationDate(reservation.getReservationDate());
        reservations.setExpectedrentaldate(reservation.getExpectedrentaldate());
        reservations.setCarId(reservation.getCarId());
        reservations.setReservationCompleted(reservation.isReservationCompleted());
        reservations.setReservationStatus(reservation.getReservationStatus());
        return 1;
      }
    }
    return 0;
  }
}
