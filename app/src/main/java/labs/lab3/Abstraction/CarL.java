package labs.lab3.Abstraction;

import java.util.ArrayList;
import labs.lab3.Car;

public class CarL extends Lists<Car>{

  @Override
  public int add(Car car) {
    if (arrayCar.add(car)) {
      return 1;
    }

    return 0;
  }

  @Override
  public int remove(int id) {
    for (int i = 0; i < arrayCar.size(); i++) {
      if (arrayCar.get(i).getcarId() == id) {
        arrayCar.remove(i);
        return 1;
      }
    }
    return 0;
  }

  @Override
  public int edit(int id,Car car) {
    for (Car cars : arrayCar) {
      if (cars.getcarId() == id) {
        cars.setBrand(car.getBrand());
        cars.setModel(car.getModel());
        cars.setYear(car.getYear());
        cars.setMileage(car.getMileage());
        cars.setAvailability(car.isAvailability());
        return 1;
      }
    }
    return 0;
  }

}
