package labs.lab3;

import labs.lab3.Enums.rStatus;
/**
 * Clase que representa una reservación.
 */
public class Reservation {

  private int reservationId;
  private int clientId;
  private String reservationDate;
  private String expectedrentaldate;
  private int carId;
  private boolean reservationCompleted;
  private rStatus reservationStatus;

  public Reservation(int reservationId, int clientId, String reservationDate,
                     String expectedrentaldate, int carId, boolean reservationCompleted,
                     rStatus reservationStatus) {
    this.reservationId = reservationId;
    this.clientId = clientId;
    this.reservationDate = reservationDate;
    this.expectedrentaldate = expectedrentaldate;
    this.carId = carId;
    this.reservationCompleted = reservationCompleted;
    this.reservationStatus = reservationStatus;
  }

  public int getReservationId() {
    return reservationId;
  }

  public void setReservationId(int reservationId) {
    this.reservationId = reservationId;
  }

  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  public String getReservationDate() {
    return reservationDate;
  }

  public void setReservationDate(String reservationDate) {
    this.reservationDate = reservationDate;
  }

  public String getExpectedrentaldate() {
    return expectedrentaldate;
  }

  public void setExpectedrentaldate(String expectedrentaldate) {
    this.expectedrentaldate = expectedrentaldate;
  }

  public int getCarId() {
    return carId;
  }

  public void setCarId(int carId) {
    this.carId = carId;
  }

  public boolean isReservationCompleted() {
    return reservationCompleted;
  }

  public void setReservationCompleted(boolean reservationCompleted) {
    this.reservationCompleted = reservationCompleted;
  }

  public rStatus getReservationStatus() {
    return reservationStatus;
  }

  public void setReservationStatus(rStatus reservationStatus) {
    this.reservationStatus = reservationStatus;
  }
}
