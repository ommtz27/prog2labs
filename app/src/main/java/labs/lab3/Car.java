package labs.lab3;
/**
 * Clase que representa un coche.
 */
public class Car {

  private int carId;
  private String brand;
  private String model;
  private String year;
  private String mileage;
  private boolean availability;

  public Car(int carId, String brand, String model, String year, String mileage, boolean availability) {
    this.carId = carId;
    this.brand = brand;
    this.model = model;
    this.year = year;
    this.mileage = mileage;
    this.availability = availability;
  }

  public int getcarId() {
    return carId;
  }

  public void setCarId(int carId) {
    this.carId = carId;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getMileage() {
    return mileage;
  }

  public void setMileage(String mileage) {
    this.mileage = mileage;
  }

  public boolean isAvailability() {
    return availability;
  }

  public void setAvailability(boolean availability) {
    this.availability = availability;
  }

}
