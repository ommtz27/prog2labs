package labs.lab3;

import java.util.ArrayList;
/**
 * Clase que representa un cliente.
 */
public class Client {
  private int clientId;
  private String clientName;
  private String clientPhone;
  private String clientAddress;
  private int licenseId;
  private String licenseExpedition;
  private int licenseYears;
  private ArrayList<Rental> rentalRecord;

  public Client(int clientId, String clientName, String clientPhone, String clientAddress,
                int licenseId, String licenseExpedition, int licenseYears) {
    this.clientId = clientId;
    this.clientName = clientName;
    this.clientPhone = clientPhone;
    this.clientAddress = clientAddress;
    this.licenseId = licenseId;
    this.licenseExpedition = licenseExpedition;
    this.licenseYears = licenseYears;
    this.rentalRecord = new ArrayList<>();
  }

  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public String getClientPhone() {
    return clientPhone;
  }

  public void setClientPhone(String clientPhone) {
    this.clientPhone = clientPhone;
  }

  public String getClientAddress() {
    return clientAddress;
  }

  public void setClientAddress(String clientAddress) {
    this.clientAddress = clientAddress;
  }

  public int getLicenseId() {
    return licenseId;
  }

  public void setLicenseId(int licenseId) {
    this.licenseId = licenseId;
  }

  public String getLicenseExpedition() {
    return licenseExpedition;
  }

  public void setLicenseExpedition(String licenseExpedition) {
    this.licenseExpedition = licenseExpedition;
  }

  public int getLicenseYears() {
    return licenseYears;
  }

  public void setLicenseYears(int licenseYears) {
    this.licenseYears = licenseYears;
  }

  public ArrayList<Rental> getRentalRecord() {
    return rentalRecord;
  }

  public void setRentalRecord(ArrayList<Rental> rentalRecord) {
    this.rentalRecord = rentalRecord;
  }
}
