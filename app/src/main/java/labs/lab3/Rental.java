package labs.lab3;

import labs.lab3.Enums.Insurance;
/**
 * Clase que representa un alquiler.
 */
public class Rental {

  private int rentalId;
  private int reservationId;
  private String conditionsTerms;
  private String rentalStartDate;
  private String rentalEndDate;
  private double rentalPrice;
  private Insurance rentalInsuranceType;

  public Rental(int rentalId, int reservationId, String conditionsTerms, String rentalStartDate,
                String rentalEndDate, double rentalPrice, Insurance rentalInsuranceType) {
    this.rentalId = rentalId;
    this.reservationId = reservationId;
    this.conditionsTerms = conditionsTerms;
    this.rentalStartDate = rentalStartDate;
    this.rentalEndDate = rentalEndDate;
    this.rentalPrice = rentalPrice;
    this.rentalInsuranceType = rentalInsuranceType;
  }

  public int getRentalId() {
    return rentalId;
  }

  public void setRentalId(int rentalId) {
    this.rentalId = rentalId;
  }

  public int getReservationId() {
    return reservationId;
  }

  public void setReservationId(int reservationId) {
    this.reservationId = reservationId;
  }

  public String getConditionsTerms() {
    return conditionsTerms;
  }

  public void setConditionsTerms(String conditionsTerms) {
    this.conditionsTerms = conditionsTerms;
  }

  public String getRentalStartDate() {
    return rentalStartDate;
  }

  public void setRentalStartDate(String rentalStartDate) {
    this.rentalStartDate = rentalStartDate;
  }

  public String getRentalEndDate() {
    return rentalEndDate;
  }

  public void setRentalEndDate(String rentalEndDate) {
    this.rentalEndDate = rentalEndDate;
  }

  public double getRentalPrice() {
    return rentalPrice;
  }

  public void setRentalPrice(double rentalPrice) {
    this.rentalPrice = rentalPrice;
  }

  public Insurance getRentalInsuranceType() {
    return rentalInsuranceType;
  }

  public void setRentalInsuranceType(Insurance rentalInsuranceType) {
    this.rentalInsuranceType = rentalInsuranceType;
  }
}
