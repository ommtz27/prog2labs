package labs;

import labs.lab3.Abstraction.CarL;
import labs.lab3.Abstraction.ClientL;
import labs.lab3.Abstraction.RentalL;
import labs.lab3.Abstraction.ReservationL;
import labs.lab3.Enums.Insurance;
import labs.lab3.Enums.rStatus;
import labs.lab3.Rental;
import labs.lab3.Reservation;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClientRentalTest {

  private RentalL rentalList = new RentalL();
  private ReservationL reservationList = new ReservationL();


  @Test
  void createReservation(){
    var defaultReservation = new Reservation(1,1,"2024-01-20","2024-02-01",1,false, rStatus.WAITING);
    assertEquals(1,reservationList.add(defaultReservation));
  }


  @Test
  void deleteReservation(){
    var reservation = new Reservation(2,1,"2024-01-24","2024-02-01",1,false,rStatus.WAITING);
    assertEquals(1,reservationList.add(reservation));
    assertEquals(1,reservationList.remove(2));
  }

  @Test
  void updateReservation(){
    var reservation = new Reservation(2,1,"2024-01-24","2024-02-01",1,false,rStatus.WAITING);
    assertEquals(1,reservationList.edit(1,reservation));
  }
  @Test
  void createRental(){
    var reservation = new Reservation(2,1,"2024-01-24","2024-02-01",1,false,rStatus.WAITING);
    var rental = new Rental(1,2,"Lorem ipsum dolor sit amet, consectetur adipiscing elit",reservation.getExpectedrentaldate(),"2024-06-01",5000,
        Insurance.COMPREHENSIVE_INSURANCE);
    assertEquals(1,reservationList.add(reservation));
    assertEquals(1,rentalList.add(rental));
  }

}
