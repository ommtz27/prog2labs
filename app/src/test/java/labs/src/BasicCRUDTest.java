package labs.src;

import labs.lab3.Abstraction.CarL;
import labs.lab3.Abstraction.ClientL;
import labs.lab3.Car;
import labs.lab3.Client;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicCRUDTest  {

  private CarL carlist = new CarL();
  private ClientL clientlist = new ClientL();

  @Test
  void Registration(){
    var car8 = new Car(8, "Volkswagen", "Jetta", "2022", "7,500 miles", true);
    assertEquals(1,carlist.add(car8));
    var client8 = new Client(8, "Samantha Wilson", "555-7890", "876 Willow St", 98765, "2020-07-12", 3);
    assertEquals(1,clientlist.add(client8));
  }

  @Test
  void Update(){
    var car8 = new Car(8, "Volkswagen", "Jetta", "2022", "7,500 miles", true);
    assertEquals(1,carlist.edit(1,car8));
    var client8 = new Client(8, "Samantha Wilson", "555-7890", "876 Willow St", 98765, "2020-07-12", 3);
    assertEquals(1,clientlist.edit(1,client8));
  }

  @Test
  void Delete(){
    assertEquals(1,carlist.remove(1));
    assertEquals(1,clientlist.remove(1));
  }

}
